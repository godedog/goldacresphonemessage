// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

/**
 * Global variable containing the query we'd like to pass to Flickr. In this
 * case, kittens!
 *
 * @type {string}
 */
$(document).ready(function() {




			$('#send').bind('click', function (e, info) {
			
			  var DataArray =  $('#form').serializeArray();
			  
			  

				$.ajax({
						url: "http://portal.goldacres.net.au/app/ReceptionAjax.php",
						global: false,
						type: "POST",
						data: ({
						"Actions": "EMAIL",
						adata:DataArray
						}),
						dataType: "html",
							success: function (new_data) {
							console.log(new_data)
								$(".alert").show();
							}
						});
						
						return false;
				
																
																
																
				
			});

 $(".form_datetime").datetimepicker({format: 'dd-mm-yy hh:ii'});
 

                              
 
 
  $('#target_email').select2({
    width: 300,
    allowClear:true,
    placeholder: "Search for email",
    minimumInputLength: 3,
    initSelection : function (element, callback) {
        var data = {id: element.val(), text: element.val()};
        callback(data);
    },
    
    ajax: {
        url: 'http://portal.goldacres.net.au/app/ReceptionAjax.php',
        dataType: 'json',
        params: {
            method: 'post'
        },
        data: function (term, page) {
             return {
                        q: term, // search term
                        page_limit: 10,
                        ACTIONS:'JSONDATA',
                     };
        },
        results: function(data, page) {
            return { results: data };
        }
        , ACTIONS:'JSONDATA'
    }
});

	$('#target_email').change(function() {
      
      $('#target_email_id').val($('#target_email').select2('data').email);

      
      
});

});